const express = require('express')
const fs = require('fs')
const bodyParser = require('body-parser')
const app = express()
const FILE_AKTIVNOSTI = "private/aktivnosti.txt"
const FILE_PREDMETI = "private/predmeti.txt"

const isActivityValid = (aktivnost) => {
    if (typeof aktivnost.naziv === "string" &&
        typeof aktivnost.tip === "string" &&
        typeof aktivnost.pocetak === "number" &&
        typeof aktivnost.kraj === "number" &&
        typeof aktivnost.dan === "string" &&
        aktivnost.naziv !== "" &&
        aktivnost.tip !== "" &&
        ["PONEDJELJAK", "UTORAK", "SRIJEDA", "ČETVRTAK", "CETVRTAK", "PETAK", "SUBOTA", "NEDJELJA"].includes(aktivnost.dan.toUpperCase()) &&
        aktivnost.pocetak >= 8 &&
        aktivnost.kraj <= 20 &&
        aktivnost.kraj > aktivnost.pocetak) {

        if ((aktivnost.pocetak != parseInt(aktivnost.pocetak) && aktivnost.pocetak - parseInt(aktivnost.pocetak) !== 0.5)
            || (aktivnost.kraj != parseInt(aktivnost.kraj) && aktivnost.kraj - parseInt(aktivnost.kraj) !== 0.5))
            return false

        const data = String(fs.readFileSync(FILE_AKTIVNOSTI, "utf-8"))
        const aktivnosti = parseActivitiesFile(data)

        for (let i = 0; i < aktivnosti.length; i++) {
            if (aktivnost.dan.toUpperCase() === aktivnosti[i].dan.toUpperCase() &&
                (aktivnost.pocetak >= aktivnosti[i].pocetak && aktivnost.kraj <= aktivnosti[i].kraj
                    || aktivnost.pocetak < aktivnosti[i].pocetak && aktivnost.kraj > aktivnosti[i].pocetak
                    || aktivnost.pocetak < aktivnosti[i].kraj && aktivnost.kraj > aktivnosti[i].kraj))
                return false
        }
        return true
    } else
        return false
}

const parseSubjectsFile = (data) => {
    let newDataArray = String(data)
        .split(/[\n\r]+/g)
        .filter(s => s !== "")
    let json = []
    newDataArray.forEach(predmet => {
        let jsonObject = {}
        jsonObject.naziv = predmet
        json.push(jsonObject)
    })
    return json
}

const parseActivitiesFile = (data, naziv = null) => {
    let newDataArray = String(data)
        .split(/[\n\r]+/g)
        .filter(s => s !== "")
    let json = []
    newDataArray.forEach(aktivnost => {
        let newActivity = aktivnost.split(",")
        let jsonObject = {}

        jsonObject.naziv = newActivity[0]
        jsonObject.tip = newActivity[1]
        jsonObject.pocetak = Number(newActivity[2])
        jsonObject.kraj = Number(newActivity[3])
        jsonObject.dan = newActivity[4]

        if (naziv === null || jsonObject.naziv === naziv.toUpperCase())
            json.push(jsonObject)
    })
    return json
}

app.use(express.static('public'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.get("/predmeti", (req, res) => {
    fs.readFile(FILE_PREDMETI, (err, data) => {
        if (err) throw err
        res.json(parseSubjectsFile(data))
    })
})

app.get("/aktivnosti", (req, res) => {
    fs.readFile(FILE_AKTIVNOSTI, (err, data) => {
        if (err) throw err
        res.json(parseActivitiesFile(data))
    })
})

app.get("/predmet/:naziv/aktivnost", (req, res) => {
    fs.readFile(FILE_AKTIVNOSTI, (err, data) => {
        if (err) throw err
        res.json(parseActivitiesFile(data, req.params.naziv))
    })
})

app.post("/predmet", (req, res) => {
    const predmet = req.body.naziv

    fs.readFile(FILE_PREDMETI, (errR, data) => {
        if (errR) throw errR

        const newData = String(data).split(/[\s]+/g)
        if (newData.includes(predmet.toUpperCase()))
            res.json({message: "Naziv predmeta postoji!"})
        else {
            fs.appendFile(FILE_PREDMETI, predmet.toUpperCase() + "\n", (errW) => {
                if (errW) throw errW
                res.json({message: "Uspješno dodan predmet!"})
            });
        }
    })
})


app.post("/aktivnost", (req, res) => {
    const aktivnost = req.body
    aktivnost.pocetak = Number(aktivnost.pocetak)
    aktivnost.kraj = Number(aktivnost.kraj)
    if (!isActivityValid(aktivnost)) {
        res.status(400)
        res.json({message: "Aktivnost nije validna!"})
    } else {
        const novaAktivnost = aktivnost["naziv"] + ","
            + aktivnost["tip"] + ","
            + aktivnost["pocetak"] + ","
            + aktivnost["kraj"] + ","
            + aktivnost["dan"] + "\n"
        fs.appendFile(FILE_AKTIVNOSTI, novaAktivnost.toUpperCase(), (err) => {
            if (err) throw err
            res.json({message: "Uspješno dodana aktivnost!"})
        });
    }
})

app.delete("/aktivnost/:naziv", (req, res) => {
    const naziv = req.params.naziv.toUpperCase()

    fs.readFile(FILE_AKTIVNOSTI, (errR, data) => {
        if (errR) {
            res.json({message: "Greška - aktivnost nije obrisana!"})
            throw errR
        }

        let newDataArray = String(data).split(/[,\s]+/g)
        let isFound = false

        for (let i = 0; i < newDataArray.length; i += 5)
            if (newDataArray[i] === naziv) {
                isFound = true
                newDataArray.splice(i, 5)
                i -= 5
            }
        if (!isFound)
            res.json({message: "Greška - aktivnost nije obrisana!"})

        else {
            let newData = ""
            for (let i = 0; i < newDataArray.length; i += 5)
                newData += newDataArray.slice(i, i + 5).join(",") + "\n"

            fs.writeFile(FILE_AKTIVNOSTI, newData, errW => {
                if (errW) throw errW
                res.json({message: "Uspješno obrisana aktivnost!"})
            })
        }
    })
})

app.delete("/predmet/:naziv", (req, res) => {
    const naziv = req.params.naziv.toUpperCase()

    fs.readFile(FILE_PREDMETI, (errR, data) => {
        if (errR) {
            res.json({message: "Greška - predmet nije obrisan!"})
            throw errR
        }

        let newDataArray = String(data).split(/[\s]+/g)
        let isFound = false

        const index = newDataArray.indexOf(naziv);
        if (index > -1) {
            isFound = true
            newDataArray.splice(index, 1);
        }
        if (!isFound)
            res.json({message: "Greška - predmet nije obrisan!"})

        else {
            let newData = newDataArray.join("\n")
            fs.writeFile(FILE_PREDMETI, newData, errW => {
                if (errW) {
                    res.json({message: "Greška - predmet nije obrisan!"})
                    throw errW
                }
                res.json({message: "Uspješno obrisan predmet!"})
            })
        }
    })
})

app.delete("/all", (req, res) => {
    fs.writeFile(FILE_PREDMETI, "", err => {
        if (err) {
            res.json({message: "Greška - sadržaj datoteka nije moguće obrisati!"})
            throw err
        }

        fs.writeFile(FILE_AKTIVNOSTI, "", err => {
            if (err) {
                res.json({message: "Greška - sadržaj datoteka nije moguće obrisati!"})
                throw err
            }
            else {
                res.json({message: "Uspješno obrisan sadržaj datoteka!"})
            }
        })

    })
})

app.listen(3000)
module.exports = app