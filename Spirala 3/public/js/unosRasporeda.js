let PREDMETI = []
let AKTIVNOSTI = []

window.addEventListener("load", () => {
    popuniTabele()
})

const pretvoriVrijeme = (vrijeme) => {
    vrijeme = vrijeme.replace(":", ".")
    if(vrijeme.substr(3, 2) === "00")
        return vrijeme.substr(0, 2)
    else if(vrijeme.substr(3, 2) === "30")
        return vrijeme.substr(0, 3) + "5"
    return vrijeme + "1"
}

const obrisiNoviPredmet = (predmet) => {
    const delPredmet = new XMLHttpRequest()

    delPredmet.onreadystatechange = () => {
        if (delPredmet.readyState === 4 && delPredmet.status === 200)
            console.log("Novi dodani predmet obirsan !")
        else if (delPredmet.readyState === 4 && delPredmet.status === 404)
            console.log("Novi dodani predmet nije obrisan, GRESKA !")
    }

    delPredmet.open("DELETE", "http://localhost:3000/predmet/" + predmet)
    delPredmet.send()
}

const popuniTabele = () => {
    const reqPredmeti = new XMLHttpRequest()
    const reqAktivnosti = new XMLHttpRequest()
    document.getElementById("predmeti__ul").innerHTML = ""
    document.getElementById("aktivnosti__table").innerHTML = ""

    reqPredmeti.onreadystatechange = () => {
        if (reqPredmeti.readyState === 4 && reqPredmeti.status === 200) {
            PREDMETI = JSON.parse(reqPredmeti.responseText)
            for(let i = 0; i < PREDMETI.length; i++)
                document.getElementById("predmeti__ul").innerHTML += "<li>" + PREDMETI[i].naziv + "</li>"
        }
        else if (reqPredmeti.readyState === 4 && reqPredmeti.status === 404)
            console.log("GRESKA")
    }

    reqAktivnosti.onreadystatechange = () => {
        if (reqAktivnosti.readyState === 4 && reqAktivnosti.status === 200) {
            AKTIVNOSTI = JSON.parse(reqAktivnosti.responseText)
            for(let i = 0; i < AKTIVNOSTI.length; i++) {
                document.getElementById("aktivnosti__table").innerHTML +=
                    "<tr>"
                    + "<td>" + AKTIVNOSTI[i].naziv + "</td>"
                    + "<td>" + AKTIVNOSTI[i].tip + "</td>"
                    + "<td>" + AKTIVNOSTI[i].pocetak + "</td>"
                    + "<td>" + AKTIVNOSTI[i].kraj + "</td>"
                    + "<td>" + AKTIVNOSTI[i].dan + "</td>" +
                    "</tr>"
            }
        }
        else if (reqAktivnosti.readyState === 4 && reqAktivnosti.status === 404)
            console.log("GRESKA")
    }

    reqPredmeti.open("GET", "http://localhost:3000/predmeti")
    reqPredmeti.send()
    reqAktivnosti.open("GET", "http://localhost:3000/aktivnosti")
    reqAktivnosti.send()
}

const validationAction = (isValid, poruka) => {
    const validacija = document.getElementById("validacija")

    validacija.style.display = "flex";
    isValid ? validacija.style.backgroundColor = "lightgreen" : validacija.style.backgroundColor = "lightcoral";
    validacija.innerText = poruka
    setTimeout(() => {
        validacija.style.display = "none"
    }, 2000)
}

const asyncPostNewActivity = (dodaniPredmet = undefined) => {
    const urlEncodedReq = new XMLHttpRequest();

    let params = "naziv=" + document.getElementById("naziv").value + "&"
        + "tip=" + document.getElementById("tip").value + "&"
        + "pocetak=" + pretvoriVrijeme(document.getElementById("pocetak").value) + "&"
        + "kraj=" + pretvoriVrijeme(document.getElementById("kraj").value) + "&"
        + "dan=" + document.getElementById("dan").value

    urlEncodedReq.open('POST', "http://localhost:3000/aktivnost", true);
    urlEncodedReq.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    urlEncodedReq.onreadystatechange = () => {
        if(urlEncodedReq.readyState === 4 && urlEncodedReq.status === 200){
            console.log(urlEncodedReq.responseText)
            validationAction(true, "Aktivnost je uspješno zapisana !")
            popuniTabele()
        }
        else if(urlEncodedReq.readyState === 4 && urlEncodedReq.status === 400) {
            validationAction(false, "Aktivnost nije validna !")
            if(dodaniPredmet !== undefined)
                obrisiNoviPredmet(dodaniPredmet)
        }
    }

    urlEncodedReq.send(params);
}

document.getElementById("button").addEventListener("click", () => {
    const noviPredmet = document.getElementById("naziv").value.toUpperCase()

    if(PREDMETI.every(obj => obj.naziv !== noviPredmet)) {
        const postPredmet = new XMLHttpRequest()

        postPredmet.onreadystatechange = () => {
            if (postPredmet.readyState === 4 && postPredmet.status === 200) {
                asyncPostNewActivity(noviPredmet)
            }
            else if (postPredmet.readyState === 4 && postPredmet.status === 404) {
                validationAction(false, "Greška pri zapisivanju predmeta !")
            }
        }
        postPredmet.open("POST", "http://localhost:3000/predmet")
        postPredmet.setRequestHeader('Content-type', 'application/json; charset=utf-8');
        postPredmet.send(JSON.stringify({"naziv": noviPredmet}))
    }
    else
        asyncPostNewActivity()
})


